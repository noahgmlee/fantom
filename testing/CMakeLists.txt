cmake_minimum_required(VERSION 3.17)
project(testing)

set(CMAKE_CXX_STANDARD 14)

include_directories(src)

add_executable(testing
        src/Bishop.cpp
        src/Empty.cpp
        src/GameClass.cpp
        src/GameClass.h
        src/King.cpp
        src/Knight.cpp
        src/Pawn.cpp
        src/Piece.cpp
        src/Piece.h
        src/Queen.cpp
        src/Rook.cpp
        src/testingGameClass.cpp)
