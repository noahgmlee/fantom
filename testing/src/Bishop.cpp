/*
	Bishop.cpp
	Created By: Naod Dereje
	Date: 11.3.2020
*/

#include "Piece.h"
#include "GameClass.h"
bool Bishop::validMove(int x, int y) {
    int currX = this->getX();
    int currY = this->getY();
    if (x > 7 || x < 0 || y > 7 || y < 0){
        return false;
    }
    if (currX == x && currY == y)
        return false;

    if (abs(x - currX) == abs(y - currY)){
        if (currX < x && currY < y){
            while (currX < x - 1){
                currX++;
                currY++;
                if (isOccupied(currX, currY)){
                    return false;
                }
            }
        }
        else if (currX < x && currY > y){
            while (currX < x - 1){
                currX++;
                currY--;
                if (isOccupied(currX, currY)){
                    return false;
                }
            }
        }
        else if (currX > x && currY < y){
            while (currX > x + 1){
                currX--;
                currY++;
                if (isOccupied(currX, currY)){
                    return false;
                }
            }
        }
        else if (currX > x && currY > y){
            while (currX > x + 1){
                currX--;
                currY--;
                if (isOccupied(currX, currY)){
                    return false;
                }
            }
        }
    }
    if (abs(x - currX) == abs(y - currY)){
        if (isOccupied(x,y)){
            if (pieceAt(x,y)->isWhite == this->isWhite){
                return false;
            }
            return true;
        }
        return true;
    }
    return false;
}
