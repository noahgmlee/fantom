/* Created on : Nov. 05, 2020
* Author : Thierry Jones */

#include "Piece.h"
#include "GameClass.h"

//Piece* check[8][8] = {};


bool King::validMove(int x, int y) {
	int currX = this->getX();
	int currY = this->getY();
	if (x > 7 || x < 0 || y > 7 || y < 0){
		return false;
	}
	if (currX == x && currY == y) {
		return false;
	}

	if ((pieceAt(x, y)->isWhite != this->isWhite)) {
		if ((currX + 1 == x && currY == y) || (currX - 1 == x && currY == y) ||
			(currX == x && currY + 1 == y) || (currX == x && currY - 1 == y) || (currX - 1 == x && currY - 1 == y) ||
			(currX - 1 == x && currY + 1 == y) || (currX + 1 == x && currY - 1 == y) || (currX + 1 == x && currY + 1 == y)) {//all directions +1
			this->isFirstMove = false;
			return true;
		}
	}
	else if (pieceAt(x, y)->isWhite == this->isWhite && (pieceAt(x,y)->abbr == 'r' || pieceAt(x, y)->abbr == 'R')
			&& this->isFirstMove && pieceAt(x, y)->isFirstMove){
		if (currY > y){
			while (currY > 1){
				currY--;
				if(isOccupied(x, currY))
					return false;
			}
			isFirstMove = false;
			return true;
		}
		else{
			while (currY < 6){
				currY++;
				if(isOccupied(x, currY))
					return false;
			}
			isFirstMove = false;
			return true;
		}
	}
	return false;
}

//This function will determine if the King is in check
//General strategy is to poll every single possible path of attack from an opponent to the king and check if there 

bool King::inCheck(int x, int y, vector<Piece*> pieces[16]) {
	if (opponentMoves(x, y, pieces)) { // opposite team piece validMove puts king in check then king cannot move to that square
		return true;
	}
	return false;
}
