/*
 * Pawn.cpp
 *
 *  Created on: Nov. 04, 2020
 *      Author: Nicolas Gesualdo
 */

#include "Piece.h"
#include "GameClass.h"

void Pawn :: pawnUpgrade(int x, int y, char pieceUpgrade)
{
	int isWhite2 = pieceAt(x,y)->isWhite;
	chessboard[x][y] = initPiece(pieceUpgrade, x, y, isWhite2);
}

bool Pawn :: validMove(int x, int y)
{
	int currX = this->getX();
	int currY = this->getY();
	if (x > 7 || x < 0 || y > 7 || y < 0){
		return false;
	}
	if ((currX == x) && (currY == y))
	{
		return false;
	}

	if(this->isFirstMove == true) //first move, can advance one or two spaces
	{
		if((this->isWhite == 1 && (x - currX) <= 2 && currY == y && isOccupied(x,y) == false && (isOccupied(x - 1,y) == false || x-1 == currX))
				|| (this->isWhite == 0 && currX - x <= 2 && currY == y && isOccupied(x,y) == false && (isOccupied(x + 1,y) == false || x+1 == currX)))
		{
			this->isFirstMove = false;
			return true;
		}
        else if(this->isWhite == 0 && (y == currY + 1 || y == currY - 1) && x == currX - 1 && isOccupied(x,y) == true && pieceAt(x,y)->isWhite != this->isWhite)
        {
            return true;
        }
        else if((y == currY + 1 || y == currY - 1) && x == currX + 1 && isOccupied(x,y) == true && pieceAt(x,y)->isWhite != this->isWhite)
        {
            return true;
        }
	}
	else
	{
		//Any forward pawn movement after first move
		if((this->isWhite == 0 && currX-x == 1 && currY == y && isOccupied(x,y) == false)
				|| (this->isWhite == 1 && currX-x == -1 && currY == y && isOccupied(x,y) == false))
		{
			return true;
		}
		//Attack moves, two positions (adjacent diagonals) where a pawn can take an opponents piece
		else if(this->isWhite == 0 && (y == currY + 1 || y == currY - 1) && x == currX - 1 && isOccupied(x,y) == true && pieceAt(x,y)->isWhite != this->isWhite)
		{
			return true;
		}
		else if((y == currY + 1 || y == currY - 1) && x == currX + 1 && isOccupied(x,y) == true && pieceAt(x,y)->isWhite != this->isWhite)
		{
			return true;
		}
	}
	return false;
}