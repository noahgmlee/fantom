/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
  * File:   Piece.h
  * Author: tayooduyemi, Thierry Jones
  *
  * Created on October 27, 2020, 11:58 PM
  * Modified Oct 29th
  * 
  * Updated on November 23rd by Thierry Jones
  */

#ifndef PIECE_H
#define PIECE_H
#include <stdio.h>
#include <vector>
#include <string>
#include <locale>

using namespace std;

class Piece {
private:
    int x;
public:
    void setX(int x);

    void setY(int y);

private:
    int y;
public:
    Piece();
    Piece(char colour, char abbr, int isWhite);
    virtual ~Piece();
    char toString();
    Piece operator=(char c);
    int isWhite;
    bool isEmpty;
    char colour;
    char abbr;
    bool moveTo(int x, int y);
    void setPos(int x, int y);
    virtual bool opponentMoves(int x, int y, vector<Piece*> pieces[16]);
    virtual bool inCheck(int x, int y, vector<Piece*> pieces[16]);
    virtual void pawnUpgrade(int x, int y, char pieceUpgrade);
    bool isFirstMove;
    int getX();
    int getY();
    virtual bool validMove(int x, int y);
};


class Pawn : public Piece {
private:
    int x;
    int y;
public:
    Pawn(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'P' : 'p'), isWhite) {
        this->setX(x);
        this->setY(y);
        this->isFirstMove = true;
        this->isEmpty = false;
    }//If colour is white then return capital P if black return small p
    bool validMove(int x, int y);
    void pawnUpgrade(int x, int y, char pieceUpgrade); //upgrades pawn to a given piece type
    bool isFirstMove;
};

class Knight : public Piece {
public:
    Knight(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'N' : 'n'), isWhite) {
        this->setX(x);
        this->setY(y);
        this->isEmpty = false;
    }//If colour is white then return capital N if black return small n
    bool validMove(int x, int y);
};

class Bishop : public Piece {
public:
    Bishop(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'B' : 'b'), isWhite) {
        this->setX(x);
        this->setY(y);
        this->isEmpty = false; //hmm
    }//If colour is white then return capital B if black return small b
    virtual bool validMove(int x, int y);
};

class Rook : public Piece {
public:
    Rook(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'R' : 'r'), isWhite) {
        this->setX(x);
        this->setY(y);
        this->isEmpty = false;
        isFirstMove = true;
    }//If colour is white then return capital R if black return small r
    bool validMove(int x, int y);
    bool isFirstMove;
};

class Queen : public Piece {
public:
    Queen(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'Q' : 'q'), isWhite) {
        this->setX(x);
        this->setY(y);
        this->isEmpty = false;
    }//If colour is white then return capital Q if black return small q
    bool validMove(int x, int y);
};

class King : public Piece {
public:
    King(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'K' : 'k'), isWhite) {//If colour is white then return capital K if black return small k
        this->setX(x);
        this->setY(y);
        this->isEmpty = false;
        isFirstMove = true;
    }//If colour is white then return capital Q if black return small q
    bool validMove(int x, int y);
    bool inCheck(int x, int y, vector<Piece*> pieces[16]);
    bool isFirstMove;
};

class Empty : public Piece {
private:
public:
    Empty(int x, int y, int isWhite);
    bool validMove(int x, int y);
};

Piece* initPiece(char abbr, int x, int y, int isWhite);

#endif /* PIECE_H */

