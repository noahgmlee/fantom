/*
 * GameClass.h
 *
 *  Created on: Oct. 26, 2020
 *      Author: noahlee
 */
/*
 * GameClass.h additions
 *
 *  Additions made: Oct. 30, 2020
 *      Author: Nicolas Gesualdo
 */

#pragma once
#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include "Piece.h"

using namespace std;

class Game {
public:
	Game();
	void startGame();
	void resetGame();
	void saveState();
	Piece pieceAt(int x, int y);
	bool isOccupied(int x, int y);
	bool isThreatened(int x, int y);
	vector<string> isOccupied(); //holds coordinates of squares occupied with a piece
	bool hasWon;
	bool turn1;
	bool turn2;
	vector<Piece*> Team1[16];
	vector<Piece*> Team2[16];
private:
	FILE* gameFile;
	int team1wins;
	int team2wins;
};

Piece* pieceAt(int x, int y);
bool isOccupied(int x, int y);
bool isThreatened(int x, int y);
void swapPiece(int xpiece, int ypiece, int xmove, int ymove);
bool isCheckMate(vector<Piece*> pieces [16], vector<Piece*> otherpieces [16]);
extern Piece* chessboard[8][8];
