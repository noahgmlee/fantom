/*
 * Empty.cpp
 *
 *  Created on: Nov. 6, 2020
 *      Author: noahlee
 */

#include "Piece.h"

Empty :: Empty(int x, int y, int isWhite){
    this->setX(x);
    this->setY(y);
	isEmpty = true;
	this->isWhite = isWhite;
}

bool Empty :: validMove(int x, int y){
	return false;
}
