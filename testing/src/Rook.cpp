/*
 * Rook.cpp
 *
 *  Created on: Nov. 2, 2020
 *      Author: noahlee
 */

#include "Piece.h"
#include "GameClass.h"

bool Rook :: validMove(int x, int y){

	int currX = this->getX();
	int currY = this->getY();
	if ((currX != x) && (currY != y)){
		return false;
	}
	if ((currX == x) && (currY == y)){
		return false;
	}
	if (x > 7 || x < 0 || y > 7 || y < 0){
		return false;
	}
	if (currX != x)	{
		if (currX < x){
			while (currX < x - 1){
				currX++;
				if (isOccupied(currX, y)){
					return false;
				}
			}
		}
		else{
			while (currX > x + 1){
				currX--;
				if (isOccupied(currX, y)){
						return false;
				}
			}
		}
	}
	else if (currY != y) {
		if (currY < y){
			while (currY < y - 1){
				currY++;
				if (isOccupied(x, currY)){
					return false;
				}
			}
		}
		else{
			while (currY > y + 1){
				currY--;
				if (isOccupied(x, currY)){
					return false;
				}
			}
		}
	}
	if (isOccupied(x, y)){
		if (pieceAt(x, y)->isWhite == this->isWhite){
			return false;
		}
	}
	this->isFirstMove = false;
	return true;
}