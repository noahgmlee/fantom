/*
 * GameClass.cpp
 *
 *  Created on: Oct. 26, 2020
 *      Author: noahlee
 */
/*
 * GameClass.cpp additions
 *
 *  Additions made: Oct. 30, 2020
 *      Author: Nicolas Gesualdo
 */

#include "GameClass.h"
#include "Piece.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <string>

using namespace std;

Piece* chessboard[8][8] = {};

Game :: Game(){
	team1wins = 0;
	team2wins = 0;
	turn1 = true;
	turn2 = false;
	hasWon = false;
	gameFile = NULL;
}

void Game :: startGame() {
	//initialize the board
	for (int j=0; j<8; j++){
		chessboard[1][j] = initPiece('P', 1, j, 1);
		Team1->push_back(chessboard[1][j]);
		chessboard[6][j] = initPiece('p', 6, j, 0);
		Team2->push_back(chessboard[6][j]);
		if (j == 0 || j == 7){
			chessboard[0][j] = initPiece('R', 0, j, 1);
			Team1->push_back(chessboard[0][j]);
			chessboard[7][j] = initPiece('r', 7, j, 0);
			Team2->push_back(chessboard[7][j]);
		}
		else if (j== 1 || j==6){
			chessboard[0][j] = initPiece('N', 0, j, 1);
			Team1->push_back(chessboard[0][j]);
			chessboard[7][j] = initPiece('n', 7, j, 0);
			Team2->push_back(chessboard[7][j]);
		}
		else if (j==2 || j==5){
			chessboard[0][j] = initPiece('B', 0, j, 1);
			Team1->push_back(chessboard[0][j]);
			chessboard[7][j] = initPiece('b', 7, j, 0);
			Team2->push_back(chessboard[7][j]);
		}
		else if (j==3){
			chessboard[0][j] = initPiece('Q', 0, j, 1);
			Team1->push_back(chessboard[0][j]);
			chessboard[7][j] = initPiece('q', 7, j, 0);
			Team2->push_back(chessboard[7][j]);
		}
		else{
			chessboard[0][j] = initPiece('K', 0, j, 1);
			Team1->push_back(chessboard[0][j]);
			chessboard[7][j] = initPiece('k', 7, j, 0);
			Team2->push_back(chessboard[7][j]);
		}
	}
	for (int i = 2; i < 6; i++){
		for (int j= 0; j<8; j++){
			chessboard[i][j] = initPiece('0', i, j, 2);
		}
	}
}

void Game :: resetGame() {
	hasWon = false;
	team1wins = 0;
	team2wins = 0;
	for (int i = 0; i < 8; i++){
		for (int j = 0; j < 8; j++){
			chessboard[i][j] = initPiece('0', i, j, 2);
		}
	}
}

/*
void Game :: saveState() {
	gameFile = fopen("src/gameFile.txt", "w");
	if (gameFile == NULL){
		cerr << "can't open file to write" << endl;
	}
	else{
		for (int i = 0; i < 8; i++){
			if (i != 0)
					fputc('\n', gameFile);
				for (int j = 0; j < 8; j++){
					fputs(chessboard[i][j]->toString(), gameFile);
					fputc('	', gameFile);
				}
			}
	}
	fclose(gameFile);
}
*/

void swapPiece(int xpiece, int ypiece, int xmove, int ymove){
	Piece* temp = chessboard[xmove][ymove];
	chessboard[xmove][ymove] = chessboard[xpiece][ypiece];
	chessboard[xpiece][ypiece] = temp;
	chessboard[xpiece][ypiece]->setPos(xpiece, ypiece);
	chessboard[xmove][ymove]->setPos(xmove, ymove);
}

bool isCheckMate(vector<Piece*> pieces[16], vector<Piece*> otherpieces [16]){
	int j, x, y;
	Piece temp;
	for (j = 0; j < 16; j++){
		if (pieces->at(j)->abbr == 'k' || pieces->at(j)->abbr == 'K'){
			break;
		}
	}
	for (int k = 0; k < 16; k++){
		char abbr = pieces->at(k)->abbr;
		switch (abbr){
		case 'p': {
			x = pieces->at(k)->getX();
			y = pieces->at(k)->getY();
			if (pieces->at(k)->moveTo(x-1, y)){
				swapPiece(x, y, x - 1, y);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x - 1, y, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x - 1, y, x, y);
			}
			if (pieces->at(k)->moveTo(x-1, y - 1)){
				swapPiece(x, y, x - 1, y - 1);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x - 1, y - 1, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x - 1, y - 1, x, y);
			}
			if (pieces->at(k)->moveTo(x-1, y + 1)){
				swapPiece(x, y, x - 1, y + 1);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x - 1, y + 1, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x - 1, y + 1, x, y);
			}
			break;
		}
		case 'P': {
			x = pieces->at(k)->getX();
			y = pieces->at(k)->getY();
			if (pieces->at(k)->moveTo(x+1, y)){
				swapPiece(x, y, x + 1, y);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x + 1, y, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x + 1, y, x, y);
			}
			if (pieces->at(k)->moveTo(x + 1, y - 1)){
				swapPiece(x, y, x + 1, y - 1);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x + 1, y - 1, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x + 1, y - 1, x, y);
			}
			if (pieces->at(k)->moveTo(x + 1, y + 1)){
				swapPiece(x, y, x + 1, y + 1);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x + 1, y + 1, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x + 1, y + 1, x, y);
			}
			cout << "Pawn works" << endl;
			break;
		}
		case 'r': {
		case 'R':
			x = pieces->at(k)->getX();
			y = pieces->at(k)->getY();
			int rightRoom = 7 - y;
			int leftRoom = y;
			int downRoom = 7 - x;
			int upRoom = x;
			for (int i = 1; i <= rightRoom; i++){
				if (pieces->at(k)->moveTo(x, y + i)){
					swapPiece(x, y, x, y + i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x, y + i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x, y + i, x, y);

				}
				else{
					break;
				}
			}
			for (int i = 1; i <= leftRoom; i++){
				if (pieces->at(k)->moveTo(x, y - i)){
					swapPiece(x, y, x, y - i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x, y - i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x, y - i, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= downRoom; i++){
				if (pieces->at(k)->moveTo(x + i, y)){
					swapPiece(x, y, x + i, y);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x + i, y, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x + i, y, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= upRoom; i++){
				if (pieces->at(k)->moveTo(x - i, y)){
					swapPiece(x, y, x - i, y);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x - i, y, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x - i, y, x, y);
				}
				else{
					break;
				}
			}
			cout << "Rook works" << endl;
			break;
		}
		case 'n': {
		case 'N':
			x = pieces->at(k)->getX();
			y = pieces->at(k)->getY();
			if (pieces->at(k)->moveTo(x+2, y+1)){
				swapPiece(x, y, x + 2, y + 1);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x + 2, y + 1, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x + 2, y + 1, x, y);
			}
			if (pieces->at(k)->moveTo(x+2, y-1)){
				swapPiece(x, y, x + 2, y - 1);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x + 2, y - 1, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x + 2, y - 1, x, y);
			}
			if (pieces->at(k)->moveTo(x-2, y+1)){
				swapPiece(x, y, x - 2, y + 1);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x - 2, y + 1, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x - 2, y + 1, x, y);
			}
			if (pieces->at(k)->moveTo(x-2, y-1)){
				swapPiece(x, y, x - 2, y - 1);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x - 2, y - 1, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x - 2, y - 1, x, y);
			}
			if (pieces->at(k)->moveTo(x+1, y-2)){
				swapPiece(x, y, x + 1, y - 2);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x + 1, y - 2, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x + 1, y - 2, x, y);
			}
			if (pieces->at(k)->moveTo(x-1, y-2)){
				swapPiece(x, y, x - 1, y - 2);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x - 1, y - 2, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x - 1, y - 2, x, y);
			}
			if (pieces->at(k)->moveTo(x+1, y+2)){
				swapPiece(x, y, x + 1, y + 2);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x + 1, y + 2, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x + 1, y + 2, x, y);
			}
			if (pieces->at(k)->moveTo(x-1, y+2)){
				swapPiece(x, y, x - 1, y + 2);
				temp = *chessboard[x][y];
				if (!pieceAt(x, y)->isEmpty){
					*chessboard[x][y] = Empty(x, y, 2);
				}
				if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
					*chessboard[x][y] = temp;
					swapPiece(x - 1, y + 2, x, y);
					return false;
				}
				*chessboard[x][y] = temp;
				swapPiece(x - 1, y + 2, x, y);
			}
			cout << "Knight works" << endl;
			break;
		}
		case 'b': {
		case 'B':
			x = pieces->at(k)->getX();
			y = pieces->at(k)->getY();
			int rightRoom = 7 - y;
			int leftRoom = y;
			int downRoom = 7 - x;
			int upRoom = x;
			for (int i = 1; i <= leftRoom || i <= upRoom; i++){
				if (pieces->at(k)->moveTo(x - i, y - i)){
					swapPiece(x, y, x - i, y - i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x - i, y - i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x - i, y - i, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= rightRoom || i <= upRoom; i++){
				if (pieces->at(k)->moveTo(x - i, y + i)){
					swapPiece(x, y, x - i, y + i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x - i, y + i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x - i, y + i, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= leftRoom || i <= downRoom; i++){
				if (pieces->at(k)->moveTo(x + i, y - i)){
					swapPiece(x, y, x + i, y - i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x + i, y - i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x + i, y - i, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= rightRoom || i <= downRoom; i++){
				if (pieces->at(k)->moveTo(x + i, y + i)){
					swapPiece(x, y, x + i, y + i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x + i, y + i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x + i, y + i, x, y);
				}
				else{
					break;
				}
			}
			cout << "Bishop works" << endl;
			break;
		}
		case 'q': {
		case 'Q':
			x = pieces->at(k)->getX();
			y = pieces->at(k)->getY();
			int rightRoom = 7 - y;
			int leftRoom = y;
			int downRoom = 7 - x;
			int upRoom = x;
			for (int i = 1; i <= leftRoom || i <= upRoom; i++){
				if (pieces->at(k)->moveTo(x - i, y - i)){
					swapPiece(x, y, x - i, y - i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x - i, y - i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x - i, y - i, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= rightRoom || i <= upRoom; i++){
				if (pieces->at(k)->moveTo(x - i, y + i)){
					swapPiece(x, y, x - i, y + i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x - i, y + i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x - i, y + i, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= leftRoom || i <= downRoom; i++){
				if (pieces->at(k)->moveTo(x + i, y - i)){
					swapPiece(x, y, x + i, y - i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x + i, y - i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x + i, y - i, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= rightRoom || i <= downRoom; i++){
				if (pieces->at(k)->moveTo(x + i, y + i)){
					swapPiece(x, y, x + i, y + i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x + i, y + i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x + i, y + i, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= rightRoom; i++){
				if (pieces->at(k)->moveTo(x, y + i)){
					swapPiece(x, y, x, y + i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x, y + i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x, y + i, x, y);

				}
				else{
					break;
				}
			}
			for (int i = 1; i <= leftRoom; i++){
				if (pieces->at(k)->moveTo(x, y - i)){
					swapPiece(x, y, x, y - i);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x, y - i, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x, y - i, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= downRoom; i++){
				if (pieces->at(k)->moveTo(x + i, y)){
					swapPiece(x, y, x + i, y);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x + i, y, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x + i, y, x, y);
				}
				else{
					break;
				}
			}
			for (int i = 1; i <= upRoom; i++){
				if (pieces->at(k)->moveTo(x - i, y)){
					swapPiece(x, y, x - i, y);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x - i, y, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x - i, y, x, y);
				}
				else{
					break;
				}
			}
			cout << "Queen works" << endl;
			break;
		}
		case 'k': {
		case 'K':
			x = pieces->at(k)->getX();
			y = pieces->at(k)->getY();
			for(int i = -1; i <= 1; i++){
				if (pieces->at(k)->moveTo(x + i, y - 1)){
					swapPiece(x, y, x + i, y - 1);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x + i, y - 1, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x + i, y - 1, x, y);
				}
				if (pieces->at(k)->moveTo(x + i, y)){
					swapPiece(x, y, x + i, y);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x + i, y, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x + i, y, x, y);
				}
				if (pieces->at(k)->moveTo(x + i, y + 1)){
					swapPiece(x, y, x + i, y + 1);
					temp = *chessboard[x][y];
					if (!pieceAt(x, y)->isEmpty){
						*chessboard[x][y] = Empty(x, y, 2);
					}
					if (!pieces->at(j)->inCheck(pieces->at(j)->getX(), pieces->at(j)->getY(), otherpieces)){
						*chessboard[x][y] = temp;
						swapPiece(x + i, y + 1, x, y);
						return false;
					}
					*chessboard[x][y] = temp;
					swapPiece(x + i, y + 1, x, y);
				}
			}
			cout << "King works" << endl;
			break;
		}
		default: {
			break;
		}
		}
	}
	return true;
}

bool isOccupied(int x, int y)
{
	if (chessboard[x][y]->isEmpty){
		return false;
	}
	return true;
}

Piece* pieceAt(int x, int y){
	return chessboard[x][y];
}
