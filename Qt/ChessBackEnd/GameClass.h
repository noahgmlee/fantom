/*
 * GameClass.h
 *
 *  Created on: Oct. 26, 2020
 *      Author: noahlee
 */
 /*
  * GameClass.h additions
  *
  *  Additions made: Oct. 30, 2020
  *      Author: Nicolas Gesualdo
  */

#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include "Piece.h"

#include "Fantom_Chess_global.h"

using namespace std;

class FANTOM_CHESS_EXPORT Game {
public:
	Game();
	void startGame();
	void resetGame();
	void saveState();
	Piece pieceAt(int x, int y);
	bool isOccupied(int x, int y);
	bool isThreatened(int x, int y);
	vector<string> isOccupied(); //holds coordinates of squares occupied with a piece
	bool hasWon;
	bool turn1;
	bool turn2;
	vector<Piece*> Team1[16];
	vector<Piece*> Team2[16];
private:
	FILE* gameFile;
	int team1wins;
	int team2wins;
};

Piece* FANTOM_CHESS_EXPORT pieceAt(int x, int y);
bool FANTOM_CHESS_EXPORT isOccupied(int x, int y);
bool FANTOM_CHESS_EXPORT isThreatened(int x, int y);
void FANTOM_CHESS_EXPORT swapPiece(int xpiece, int ypiece, int xmove, int ymove);
bool FANTOM_CHESS_EXPORT isCheckMate(vector<Piece*> pieces[16], vector<Piece*> otherpieces[16]);
extern FANTOM_CHESS_EXPORT Piece* chessboard[8][8];
