/*
 * Queen.cpp
 *
 *  Created on: Nov. 05, 2020
 *      Author: Thierry Jones */

#include "Piece.h"
#include "GameClass.h"


bool Queen::validMove(int x, int y)//Can move in any direction so diagonal, horizontal and vertical
{
	int currX = this->getX();
	int currY = this->getY();
	if (x > 7 || x < 0 || y > 7 || y < 0) {
		return false;
	}
	if (currX == x && currY == y) {//Current square
		return false;
	}
	if ((currX != x && currY == y) || (currX == x && currY != y)) { // rook type check
		if (currX != x) {
			if (currX < x) {
				while (currX < x - 1) {
					currX++;
					if (isOccupied(currX, y)) {
						return false;
					}
				}
			}
			else {
				while (currX > x + 1) {
					currX--;
					if (isOccupied(currX, y)) {
						return false;
					}
				}
			}
		}
		else if (currY != y) {
			if (currY < y) {
				while (currY < y - 1) {
					currY++;
					if (isOccupied(x, currY)) {
						return false;
					}
				}
			}
			else {
				while (currY > y + 1) {
					currY--;
					if (isOccupied(x, currY)) {
						return false;
					}
				}
			}
		}
	}
	if (abs(x - currX) == abs(y - currY)) { // bishop type check
		if (currX < x && currY < y) {
			while (currX < x - 1) {
				currX++;
				currY++;
				if (isOccupied(currX, currY)) {
					return false;
				}
			}
		}
		else if (currX < x && currY > y) {
			while (currX < x - 1) {
				currX++;
				currY--;
				if (isOccupied(currX, currY)) {
					return false;
				}
			}
		}
		else if (currX > x && currY < y) {
			while (currX > x - 1) {
				currX--;
				currY++;
				if (isOccupied(currX, currY)) {
					return false;
				}
			}
		}
		else if (currX > x && currY > y) {
			while (currX > x + 1) {
				currX--;
				currY--;
				if (isOccupied(currX, currY)) {
					return false;
				}
			}
		}
	}
	if ((abs(x - currX) == abs(y - currY)) || ((currX != x && currY == y) || (currX == x && currY != y))) {
		if (isOccupied(x, y)) {
			if (pieceAt(x, y)->isWhite == this->isWhite) {
				return false;
			}
		}
		return true;
	}
	return false;
}

bool Queen::moveTo(int x, int y) {
	if (validMove(x, y)) {
		return true;
	}
	else
		return false;
}

int Queen::getX() {
	return this->x;
}

int Queen::getY() {
	return this->y;
}

void Queen::setPos(int x, int y) {
	this->x = x;
	this->y = y;
}
