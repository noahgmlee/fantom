/*
 * Empty.cpp
 *
 *  Created on: Nov. 6, 2020
 *      Author: noahlee
 */

#include "Piece.h"

Empty::Empty(int x, int y, int isWhite) {
	this->x = x;
	this->y = y;
	isEmpty = true;
	this->isWhite = isWhite;
}

bool Empty::moveTo(int x, int y) {
	if (validMove(x, y)) {
		return true;
	}
	else
		return false;
}

bool Empty::validMove(int x, int y) {
	return false;
}

void Empty::setPos(int x, int y) {
	this->x = x;
	this->y = y;
}

int Empty::getX() {
	return this->x;
}

int Empty::getY() {
	return this->y;
}
