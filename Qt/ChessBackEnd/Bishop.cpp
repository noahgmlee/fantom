/*
	Bishop.cpp
	Created By: Naod Dereje
	Date: 11.3.2020
*/

#include "Piece.h"
#include "GameClass.h"

int Bishop::getX() {
	return this->x;
}
int Bishop::getY() {
	return this->y;
}

bool Bishop::validMove(int x, int y) {
	int currX = this->getX();
	int currY = this->getY();
	if (x > 7 || x < 0 || y > 7 || y < 0) {
		return false;
	}
	if (currX == x && currY == y)
		return false;

	if (abs(x - currX) == abs(y - currY)) {
		if (currX < x && currY < y) {
			while (currX < x - 1) {
				currX++;
				currY++;
				if (isOccupied(currX, currY)) {
					return false;
				}
			}
		}
		else if (currX < x && currY > y) {
			while (currX < x - 1) {
				currX++;
				currY--;
				if (isOccupied(currX, currY)) {
					return false;
				}
			}
		}
		else if (currX > x && currY < y) {
			while (currX > x - 1) {
				currX--;
				currY++;
				if (isOccupied(currX, currY)) {
					return false;
				}
			}
		}
		else if (currX > x && currY > y) {
			while (currX > x - 1) {
				currX--;
				currY--;
				if (isOccupied(currX, currY)) {
					return false;
				}
			}
		}
	}
	if (abs(x - currX) == abs(y - currY)) {
		if (isOccupied(x, y)) {
			if (pieceAt(x, y)->isWhite == this->isWhite) {
				return false;
			}
			return true;
		}
		return true;
	}
	return false;
}

bool Bishop::moveTo(int x, int y) {
	if (validMove(x, y)) {
		return true;
	}
	else
		return false;
}

void Bishop::setPos(int x, int y) {
	this->x = x;
	this->y = y;
}

