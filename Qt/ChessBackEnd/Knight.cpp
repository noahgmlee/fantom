/*
 * Knight.cpp
 *
 *  Created on: Nov. 10, 2020
 *      Author: noahlee
 */

#include "Piece.h"
#include "GameClass.h"

bool Knight::moveTo(int x, int y) {
	if (validMove(x, y)) {
		return true;
	}
	else
		return false;
}

bool Knight::validMove(int x, int y) {

	int currX = this->getX();
	int currY = this->getY();
	if (x > 7 || x < 0 || y > 7 || y < 0) {
		return false;
	}
	if ((currX == x || currY == y)) {
		return false;
	}
	if ((abs(currX - x) == 2 && abs(currY - y) == 1) || (abs(currX - x) == 1 && abs(currY - y) == 2)) {
		if (isOccupied(x, y)) {
			if (pieceAt(x, y)->isWhite == this->isWhite) {
				return false;
			}
			return true;
		}
		else {
			return true;
		}
	}
	return false;
}

int Knight::getX() {
	return this->x;
}

int Knight::getY() {
	return this->y;
}

void Knight::setPos(int x, int y) {
	this->x = x;
	this->y = y;
}


