
 /*
  * File:   Piece.h
  * Author: tayooduyemi, Thierry Jones
  *
  * Created on October 27, 2020, 11:58 PM
  * Modified Nov 26th Thierry Jones
  */

#ifndef PIECE_H
#define PIECE_H
#include <stdio.h>
#include <vector>
#include <string>
#include <locale>

#include "Fantom_Chess_global.h"

using namespace std;

class FANTOM_CHESS_EXPORT Piece {
private:
    int x;
    int y;
public:
    Piece();
    Piece(char colour, char abbr, int isWhite);
    virtual ~Piece();
    char toString();
    Piece operator=(char c);
    int isWhite;
    bool isEmpty;
    char colour;
    char abbr;
    virtual bool moveTo(int x, int y);
    virtual void setPos(int x, int y);
    virtual bool opponentMoves(int x, int y, vector<Piece*> pieces[16]);
    virtual bool inCheck(int x, int y, vector<Piece*> pieces[16]);
    virtual void pawnUpgrade(int x, int y, char pieceUpgrade);
    bool isFirstMove;
    virtual int getX();
    virtual int getY();
};


class FANTOM_CHESS_EXPORT Pawn : public Piece {
private:
    int x;
    int y;
public:
    Pawn(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'P' : 'p'), isWhite) {
        this->x = x;
        this->y = y;
        this->isFirstMove = true;
        this->isEmpty = false;
    }//If colour is white then return capital P if black return small p
    bool validMove(int x, int y);
    void pawnUpgrade(int x, int y, char pieceUpgrade); //upgrades pawn to a given piece type
    int getX();
    int getY();
    bool isFirstMove;
    bool moveTo(int x, int y);
    void setPos(int x, int y);
};

class FANTOM_CHESS_EXPORT Knight : public Piece {
private:
    int x;
    int y;
public:
    Knight(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'N' : 'n'), isWhite) {
        this->x = x;
        this->y = y;
        this->isEmpty = false;
    }//If colour is white then return capital N if black return small n
    bool validMove(int x, int y);
    int getX();
    int getY();
    bool moveTo(int x, int y);
    void setPos(int x, int y);
};

class FANTOM_CHESS_EXPORT Bishop : public Piece {
private:
    int x;
    int y;
public:
    Bishop(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'B' : 'b'), isWhite) {
        this->x = x;
        this->y = y;
        this->isEmpty = false;
    }//If colour is white then return capital B if black return small b
    virtual bool validMove(int x, int y);
    int getX();
    int getY();
    virtual bool moveTo(int x, int y);
    void setPos(int x, int y);
};

class FANTOM_CHESS_EXPORT Rook : public Piece {
private:
    int x;
    int y;
public:
    Rook(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'R' : 'r'), isWhite) {
        this->x = x;
        this->y = y;
        this->isEmpty = false;
        isFirstMove = true;
    }//If colour is white then return capital R if black return small r
    bool validMove(int x, int y);
    bool isFirstMove;
    int getX();
    int getY();
    bool moveTo(int x, int y);
    void setPos(int x, int y);
};

class FANTOM_CHESS_EXPORT Queen : public Piece {
private:
    int x;
    int y;
public:
    Queen(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'Q' : 'q'), isWhite) {
        this->x = x;
        this->y = y;
        this->isEmpty = false;
    }//If colour is white then return capital Q if black return small q
    bool validMove(int x, int y);
    int getX();
    int getY();
    bool moveTo(int x, int y);
    void setPos(int x, int y);
};

class FANTOM_CHESS_EXPORT King : public Piece {
private:
    int x;
    int y;
public:
    King(char colour, int x, int y, int isWhite) : Piece(colour, (colour == 'w' ? 'K' : 'k'), isWhite) {//If colour is white then return capital K if black return small k
        this->x = x;
        this->y = y;
        this->isEmpty = false;
        isFirstMove = true;
    }//If colour is white then return capital Q if black return small q
    bool validMove(int x, int y);
    bool inCheck(int x, int y, vector<Piece*> pieces[16]);
    int getX();
    int getY();
    bool isFirstMove;
    bool moveTo(int x, int y);
    void setPos(int x, int y);
};

class FANTOM_CHESS_EXPORT Empty : public Piece {
private:
    int x;
    int y;
public:
    Empty(int x, int y, int isWhite);
    bool moveTo(int x, int y);
    bool validMove(int x, int y);
    int getX();
    int getY();
    void setPos(int x, int y);
};

Piece* FANTOM_CHESS_EXPORT initPiece(char abbr, int x, int y, int isWhite);

#endif /* PIECE_H */

