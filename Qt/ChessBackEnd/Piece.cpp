/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
  * File:   Piece.cpp
  * Author: tayooduyemi
  *
  * Created on October 27, 2020, 11:58 PM
  *
  * Thierry Jones: Updated November 23rd
  */


#include "Piece.h"
#include "GameClass.h"

#include <stdio.h>
#include <vector>
#include<string>

  //Piece initializations
Piece::Piece() {
    colour = 'e';
    abbr = 'e';
    x = 0;
    y = 0;
    isEmpty = true;
    this->isWhite = 2;
    isFirstMove = true;
}

Piece::Piece(char colour, char abbr, int isWhite) {
    this->colour = colour;
    this->abbr = abbr;
    x = 0;
    y = 0;
    isEmpty = true;
    this->isWhite = isWhite;
    isFirstMove = true;
}

Piece :: ~Piece() {
}

bool Piece::opponentMoves(int x, int y, vector<Piece*> pieces[16]) {

    //check for each piece if the square is a validMove  if (p[i]->moveTo(x, y) == true) { return true }

    //cout << p.size() << endl;
    for (int i = 0; i < 16; i++) {
        //cout << p[i]->abbr << p[i]->getX() << ", " << p[i]->getY() << endl;
        if (pieces->at(i)->moveTo(x, y)) {//If a piece on the other teams side
            return true;
        }
    }

    return false;
};

bool Piece::inCheck(int x, int y, vector<Piece*> pieces[16]) {
    return false;
}

void Piece::pawnUpgrade(int x, int y, char pieceUpgrade) {
    return;
}

bool Piece::moveTo(int x, int y) {
    return false;
}


void Piece::setPos(int x, int y) {
    this->x = x;
    this->y = y;
}

int Piece::getX() {
    return this->x;
}

int Piece::getY() {
    return this->y;
}

Piece* initPiece(char abbr, int x, int y, int isWhite) { //initialize pieces to their own type
    char name;
    char colour;
    if (islower(abbr)) {//Lowercase will initializes piece to black
        name = abbr;
        colour = 'b';
    }
    else {//Capital switches char to lowercase and initializes piece to white
        name = std::tolower(abbr, std::locale());
        colour = 'w';
    }
    Piece* piece;
    switch (name) {
    case 'p':
        piece = new Pawn(colour, x, y, isWhite);
        break;
    case 'n':
        piece = new Knight(colour, x, y, isWhite);
        break;
    case 'b':
        piece = new Bishop(colour, x, y, isWhite);
        break;
    case 'r':
        piece = new Rook(colour, x, y, isWhite);
        break;
    case 'q':
        piece = new Queen(colour, x, y, isWhite);
        break;
    case 'k':
        piece = new King(colour, x, y, isWhite);
        break;
    default:
        piece = new Empty(x, y, isWhite);
        break;
    }
    return piece;
}

char Piece::toString() {
    return this->abbr;
}



