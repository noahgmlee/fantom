#include "chess.h"
#include "ui_chess.h"
#include <QLabel>
#include <QPixmap>
#include <QString>
#include "GameClass.h"
#include "Piece.h"

Game test;
int j;

QChar pieceNameIn;

bool turn1 = true;
bool turn2 = false;

bool validMove = false;
bool checked = false;
bool inCheck = false;
bool castleLeft = false;
bool castleRight = false;

QTabBar *TempWidget;

int xIn = -1;
int yIn = -1;

Chess :: Chess(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Chess)
{
    ui->setupUi(this);
}

Chess::~Chess()
{
    delete ui;
}


void Chess::on_pushButton_pressed()
{
    test.startGame();
    for (j = 0; j < 16; j++){
        if (test.Team1->at(j)->abbr == 'K'){
            break;
        }
    }
    ui->pushButton->setVisible(false);//closes the button

//Generate background
    QPixmap gray(":new/Images/gray.png");
    QPixmap white(":new/Images/white.jpg");

    for(int x = 0; x < 8; x++){
        for(int y = 0; y < 8; y++){
            QLabel *backgroundSquare = new QLabel();
            if((x%2==0 && y%2 ==0)){
                backgroundSquare->setPixmap(white);
            }
            else if((x%2==0 || y%2 ==0)){
                backgroundSquare->setPixmap(gray);
            }
            else{
                backgroundSquare->setPixmap(white);
            }
            ui->chessboardUI->addWidget(backgroundSquare, x, y);
        }
    }



//Team 2 -- Black pieces
     QPixmap pawnBlack(":new/Images/tamPawnBlack.png");
     QPixmap bishopBlack(":new/Images/bishop.png");
     QPixmap rookBlack(":new/Images/rook.png");
     QPixmap knightBlack(":new/Images/horse.png");
     QPixmap kingBlack(":new/Images/KingCHAD.png");
     QPixmap queenBlack(":new/Images/MowhawKing.png");

     QLabel *pawnBlack1 = new QLabel();
     QLabel *pawnBlack2 = new QLabel();
     QLabel *pawnBlack3 = new QLabel();
     QLabel *pawnBlack4 = new QLabel();
     QLabel *pawnBlack5 = new QLabel();
     QLabel *pawnBlack6 = new QLabel();
     QLabel *pawnBlack7 = new QLabel();
     QLabel *pawnBlack8 = new QLabel();
     QLabel *bishopBlack1 = new QLabel();
     QLabel *bishopBlack2 = new QLabel();
     QLabel *rookBlack1 = new QLabel();
     QLabel *rookBlack2 = new QLabel();
     QLabel *knightBlack1 = new QLabel();
     QLabel *knightBlack2 = new QLabel();
     QLabel *kingBlack1 = new QLabel();
     QLabel *queenBlack1 = new QLabel();

     pawnBlack1->setPixmap(pawnBlack);
     pawnBlack2->setPixmap(pawnBlack);
     pawnBlack3->setPixmap(pawnBlack);
     pawnBlack4->setPixmap(pawnBlack);
     pawnBlack5->setPixmap(pawnBlack);
     pawnBlack6->setPixmap(pawnBlack);
     pawnBlack7->setPixmap(pawnBlack);
     pawnBlack8->setPixmap(pawnBlack);

     bishopBlack1->setPixmap(bishopBlack);
     bishopBlack2->setPixmap(bishopBlack);

     rookBlack1->setPixmap(rookBlack);
     rookBlack2->setPixmap(rookBlack);

     knightBlack1->setPixmap(knightBlack);
     knightBlack2->setPixmap(knightBlack);
     queenBlack1->setPixmap(queenBlack);
     kingBlack1->setPixmap(kingBlack);


    ui->chessboardUI->addWidget(pawnBlack1, 1, 0);
    ui->chessboardUI->addWidget(pawnBlack2, 1, 1);
    ui->chessboardUI->addWidget(pawnBlack3, 1, 2);
    ui->chessboardUI->addWidget(pawnBlack4, 1, 3);
    ui->chessboardUI->addWidget(pawnBlack5, 1, 4);
    ui->chessboardUI->addWidget(pawnBlack6, 1, 5);
    ui->chessboardUI->addWidget(pawnBlack7, 1, 6);
    ui->chessboardUI->addWidget(pawnBlack8, 1, 7);
    ui->chessboardUI->addWidget(bishopBlack1, 0, 2);
    ui->chessboardUI->addWidget(bishopBlack2, 0, 5);
    ui->chessboardUI->addWidget(rookBlack1, 0, 0);
    ui->chessboardUI->addWidget(rookBlack2, 0, 7);
    ui->chessboardUI->addWidget(knightBlack1, 0, 6);
    ui->chessboardUI->addWidget(knightBlack2, 0, 1);
    ui->chessboardUI->addWidget(kingBlack1, 0, 4);
    ui->chessboardUI->addWidget(queenBlack1, 0, 3);


//Team 1 -- White pieces
    QPixmap pawnWhite(":new/Images/tamPawnWhite.png");
    QPixmap bishopWhite(":new/Images/bishop1.png");
    QPixmap rookWhite(":new/Images/rook1.png");
    QPixmap knightWhite(":new/Images/horse1.png");
    QPixmap kingWhite(":new/Images/ThomasDeanCHAD.png");
    QPixmap queenWhite(":new/Images/MowhawKingChad.png");


    QLabel *pawnWhite1 = new QLabel();
    QLabel *pawnWhite2 = new QLabel();
    QLabel *pawnWhite3 = new QLabel();
    QLabel *pawnWhite4 = new QLabel();
    QLabel *pawnWhite5 = new QLabel();
    QLabel *pawnWhite6 = new QLabel();
    QLabel *pawnWhite7 = new QLabel();
    QLabel *pawnWhite8 = new QLabel();

    QLabel *rookWhite1 = new QLabel();
    QLabel *rookWhite2 = new QLabel();
    QLabel *bishopWhite1 = new QLabel();
    QLabel *bishopWhite2 = new QLabel();
    QLabel *knightWhite1 = new QLabel();
    QLabel *knightWhite2 = new QLabel();

    QLabel *kingWhite1 = new QLabel();
    QLabel *queenWhite1 = new QLabel();

    //Pixmaps
    pawnWhite1->setPixmap(pawnWhite);
    pawnWhite2->setPixmap(pawnWhite);
    pawnWhite3->setPixmap(pawnWhite);
    pawnWhite4->setPixmap(pawnWhite);
    pawnWhite5->setPixmap(pawnWhite);
    pawnWhite6->setPixmap(pawnWhite);
    pawnWhite7->setPixmap(pawnWhite);
    pawnWhite8->setPixmap(pawnWhite);

    rookWhite1->setPixmap(rookWhite);
    rookWhite2->setPixmap(rookWhite);

    knightWhite1->setPixmap(knightWhite);
    knightWhite2->setPixmap(knightWhite);

    bishopWhite1->setPixmap(bishopWhite);
    bishopWhite2->setPixmap(bishopWhite);

    queenWhite1->setPixmap(queenWhite);
    kingWhite1->setPixmap(kingWhite);


    ui->chessboardUI->addWidget(pawnWhite1, 6,7);
    ui->chessboardUI->addWidget(pawnWhite2, 6,6);
    ui->chessboardUI->addWidget(pawnWhite3, 6,5);
    ui->chessboardUI->addWidget(pawnWhite4, 6,4);
    ui->chessboardUI->addWidget(pawnWhite5, 6,3);
    ui->chessboardUI->addWidget(pawnWhite6, 6,2);
    ui->chessboardUI->addWidget(pawnWhite7, 6,1);
    ui->chessboardUI->addWidget(pawnWhite8, 6,0);

    ui->chessboardUI->addWidget(rookWhite1, 7,7);
    ui->chessboardUI->addWidget(rookWhite2, 7,0);

    ui->chessboardUI->addWidget(knightWhite1, 7,6);
    ui->chessboardUI->addWidget(knightWhite2, 7,1);

    ui->chessboardUI->addWidget(bishopWhite1, 7,5);
    ui->chessboardUI->addWidget(bishopWhite2, 7,2);

    ui->chessboardUI->addWidget(queenWhite1, 7,3);
    ui->chessboardUI->addWidget(kingWhite1, 7,4);
}


void Chess::mousePressEvent(QMouseEvent *pe)
{

    if ( pe->button() == Qt::RightButton ){//piece selection
        TempWidget =(QTabBar*) qApp->widgetAt(QCursor::pos());

        QString xPos = QString::number(TempWidget->y()/67);
        QString yPos = QString::number(TempWidget->x()/67);

        xIn = xPos.toInt();
        yIn = yPos.toInt();

        pieceNameIn = chessboard[xIn][yIn]->abbr;

        QString pieceIn;
        pieceIn.push_back (pieceNameIn);

        ui->textEdit->setText(xPos + " " + yPos + " "+pieceIn);
    }

    if ( pe->button() == Qt::LeftButton){//piece movement
        QTabBar *TempWidget1 =(QTabBar*) qApp->widgetAt(QCursor::pos());

        QString yPos1 = QString::number(TempWidget1->x()/67);
        QString xPos1 = QString::number(TempWidget1->y()/67);

        int x = xPos1.toInt();
        int y = yPos1.toInt();

        QChar pieceNameOut = chessboard[x][y]->abbr;
        QString pieceOut;
        pieceOut.push_back (pieceNameOut);

            if(chessboard[xIn][yIn]->moveTo(x,y)){
                validMove = true;
                if(turn1 == false && chessboard[xIn][yIn]->isWhite == 0){//Turn check
                   ui->textEdit->setText("Not your turn, Black turn");
                }
                else if ((turn1 == true && chessboard[xIn][yIn]->isWhite == 0)){//Team 2 White
                    //Movement afterwards
                    if (pieceNameOut == 'e'&& validMove == true){//empty square
                        ui->textEdit->setText(pieceNameOut);
                        swapPiece(xIn, yIn, x, y);
                        if(pieceNameIn == 'k'){//if the king is moved into check
                            if (test.Team2->at(j)->inCheck(test.Team2->at(j)->getX(), test.Team2->at(j)->getY(), test.Team1)) {
                                validMove = false;
                                ui->textEdit->setText("You put yourself in check - White");
                                swapPiece(x, y, xIn, yIn);//swap the piece back
                            }
                        }
                        if(validMove == true){
                            ui->chessboardUI->addWidget(TempWidget, x,y);
                        }
                    }
                    else if (pieceNameOut != 'e' && validMove == true) {//enemy square
                            ui->textEdit->setText(pieceNameOut);
                            swapPiece(xIn, yIn, x, y);
                            if(pieceNameIn == 'k'){
                                if (test.Team1->at(j)->inCheck(test.Team1->at(j)->getX(), test.Team1->at(j)->getY(), test.Team2)) {
                                    validMove = false;
                                    ui->textEdit->setText("You put yourself in check - White");
                                    swapPiece(x, y, xIn, yIn);
                                }
                            }
                            if(validMove == true){
                                ui->chessboardUI->addWidget(TempWidget, x,y);
                                chessboard[xIn][yIn] = initPiece('e', x, y, 2);
                                delete TempWidget1;
                            }
                    }
                    if (test.Team1->at(j)->inCheck(test.Team1->at(j)->getX(), test.Team1->at(j)->getY(), test.Team2)) {
                        ui->textEdit->setText("You put black into check");
                        checked = true;
                    }
                    if (checked) {//check mate function
                        if (isCheckMate(test.Team1, test.Team2)) {
                            ui->textEdit->setText("CheckMate black lost - you've been DEANED");
                            test.hasWon = true;
                        }
                        else{
                            checked = false;
                        }
                    }
                    if(validMove == true){
                        turn1 = false;
                        turn2 = true;
                    }
                }
                if(turn2 == false && chessboard[xIn][yIn]->isWhite == 1){//Turn check
                    ui->textEdit->setText("Not your turn, White turn");
                }
                else if(turn2 == true && chessboard[xIn][yIn]->isWhite == 1 ){//Team 1 Black
                    //Movement afterwards
                    if (pieceNameOut == 'e' && validMove == true) {
                        ui->textEdit->setText(pieceNameOut);
                        swapPiece(xIn, yIn, x, y);
                        if(pieceNameIn == 'K'){//if the king is moved into check
                            if (test.Team1->at(j)->inCheck(test.Team1->at(j)->getX(), test.Team1->at(j)->getY(), test.Team2)) {
                                validMove = false;
                                ui->textEdit->setText("You put yourself in check - Black");
                                swapPiece(x, y, xIn, yIn);
                            }
                        }
                        if(validMove == true){
                            ui->chessboardUI->addWidget(TempWidget, x,y);
                        }
                    }
                    else if (pieceNameOut != 'e' && validMove == true){
                        ui->textEdit->setText(pieceNameOut);
                        swapPiece(xIn, yIn, x, y);
                        if(pieceNameIn == 'K'){//if the king is moved into check
                            cout<<"Black King";
                            if (test.Team1->at(j)->inCheck(test.Team1->at(j)->getX(), test.Team1->at(j)->getY(), test.Team2)) {
                                validMove = false;
                                ui->textEdit->setText("You put yourself in check - Black");
                                swapPiece(x, y, xIn, yIn);
                            }
                        }
                        if(validMove == true){
                            ui->chessboardUI->addWidget(TempWidget, x,y);
                            chessboard[xIn][yIn] = initPiece('e', x, y, 2);
                            delete TempWidget1;
                        }
                    }
                    if (test.Team2->at(j)->inCheck(test.Team2->at(j)->getX(), test.Team2->at(j)->getY(), test.Team1)) {
                        ui->textEdit->setText("You put white into check");
                        checked = true;
                    }
                    if (checked) {//check mate function
                        if (isCheckMate(test.Team2, test.Team1)) {
                            ui->textEdit->setText("CheckMate white lost - you should have anticiped this and reacted in a proper manner");
                            test.hasWon = true;
                        }
                        else{
                            checked = false;
                        }
                    }
                    if(validMove == true){
                        turn1 = true;
                        turn2 = false;
                    }
                }
            }
            else{
                ui->textEdit->setText("Bad Move");
            }
            validMove = false;
    }
}



