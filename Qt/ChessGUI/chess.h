#ifndef CHESS_H
#define CHESS_H
#include <QLabel>
#include <QWidget>
#include <QGridLayout>
#include <QMainWindow>
#include <QtWidgets>


QT_BEGIN_NAMESPACE
namespace Ui {
    class Chess;
}
QT_END_NAMESPACE

class Chess : public QMainWindow
{
    Q_OBJECT

    Ui::Chess *ui;

public:
    Chess (QWidget *parent = nullptr);
    ~Chess();

private slots:
    void on_pushButton_pressed();
    virtual void mousePressEvent(QMouseEvent* pe);
    //void on_Submit_clicked();

private:
};

#endif // CHESS_H
