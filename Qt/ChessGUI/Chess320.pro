QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    chess.cpp \

HEADERS += \
    Fantom_Chess_global.h \
    GameClass.h \
    Piece.h \
    chess.h \

FORMS += \
    chess.ui

RESOURCES += \
    Images/ChessPieces.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../build-Fantom_Chess-Desktop_Qt_5_12_5_MinGW_64_bit-Release/release/ -lFantom_Chess
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../build-Fantom_Chess-Desktop_Qt_5_12_5_MinGW_64_bit-Release/debug/ -lFantom_Chess
else:unix: LIBS += -L$$PWD/../../../build-Fantom_Chess-Desktop_Qt_5_12_5_MinGW_64_bit-Release/ -lFantom_Chess

INCLUDEPATH += $$PWD/../../../Fantom_Chess
DEPENDPATH += $$PWD/../../../Fantom_Chess
