#include "chess.h"
#include "ui_chess.h"
#include "GameClass.h"
#include "Piece.h"

#include <QApplication>
#include <QLabel>

int main(int argc, char *argv[])
{
    //Game test;
    //test.startGame();
    QApplication a(argc, argv);
    Chess board;
    board.show();
    return a.exec();
}

/*
 * testing.cpp
 *
 *  Created on: Oct. 26, 2020
 *      Author: noahlee
 *
 * Updated nov 24th Thierry Jones


#include "GameClass.h"
#include "Piece.h"
#include <iostream>
#include <stdio.h>

using namespace std;

void printBoard() {
    for (int i = 0; i < 8; i++) {
        cout << "	" << i;
    }
    cout << "\n";
    for (int i = 0; i < 8; i++) {
        if (i != 0)
            cout << '\n';
        for (int j = 0; j < 8; j++) {
            if (j == 0) {
                cout << i << "	";
            }
            cout << chessboard[i][j]->toString() << "	";
        }
    }
    cout << "\n\n\n" << endl;
}

int main() {
    Game test;
    test.startGame();
    printBoard();
    string reader;
    int xpiece;
    int ypiece;
    int xmove;
    int ymove;
    int j;
    for (j = 0; j < 16; j++) {
        if (test.Team1->at(j)->abbr == 'K') {
            break;
        }
    }

    while (!test.hasWon) {
        bool validMove = false;
        bool checked = false;
        bool castleLeft = false;
        bool castleRight = false;
        if (test.turn1) {
            cout << "team 1's turn (White)" << endl;
        }
        else {
            cout << "team 2's turn (Black)" << endl;
        }
        cout << "choose piece by typing row then col" << endl;
        cin >> reader;
        if (reader == "exit") {
            return 1;
        }
        xpiece = stoi(reader);
        cin >> reader;
        ypiece = stoi(reader);
        cout << "choose position to move to by typing row then col" << endl;
        cin >> reader;
        if (reader == "exit") {
            return 1;
        }
        xmove = stoi(reader);
        cin >> reader;
        ymove = stoi(reader);
        if ((ymove >= 0 && xmove >= 0) && (ymove < 8 && xmove < 8) && (xpiece >= 0 && ypiece >= 0) && (xpiece < 8 && ypiece < 8)) {//if piece is in range
            if ((test.turn1 && chessboard[xpiece][ypiece]->isWhite == 1) ||
                (test.turn2 && chessboard[xpiece][ypiece]->isWhite == 0)) {//if piece turn
                if (chessboard[xpiece][ypiece]->moveTo(xmove, ymove)) {
                    if (chessboard[xpiece][ypiece]->isWhite == chessboard[xmove][ymove]->isWhite) {
                        if (chessboard[xmove][ymove]->getY() == 0) { //castleLeft
                            validMove = true;
                            castleLeft = true;
                            swapPiece(xpiece, ypiece, xpiece, 2);
                            swapPiece(xmove, ymove, xmove, 3);
                        }
                        else {
                            validMove = true;
                            castleRight = true;
                            swapPiece(xpiece, ypiece, xpiece, 6);
                            swapPiece(xmove, ymove, xmove, 5);
                            cout << "just castled to the right" << endl;
                        }
                    }
                    else {
                        validMove = true;
                        swapPiece(xpiece, ypiece, xmove, ymove);
                        if (!(chessboard[xpiece][ypiece]->isEmpty)) {
                            chessboard[xpiece][ypiece] = initPiece('e', xpiece, ypiece, 2);
                        }
                    }
                }
            }
            else {
                cout << "pick your own piece" << endl;
            }
        }
        else {
            cout << "move out of bounds" << endl;
        }
        if (test.turn1) {
            if (test.Team1->at(j)->inCheck(test.Team1->at(j)->getX(), test.Team1->at(j)->getY(), test.Team2)) {
                cout << test.Team1->at(j)->abbr << " " << test.Team1->at(j)->getX() << " " << test.Team1->at(j)->getY() << endl;
                validMove = false;
                cout << "YOU PUT YOURSELF IN CHECK" << endl;
                if (castleLeft) {
                    swapPiece(xpiece, 2, xpiece, ypiece);
                    swapPiece(xmove, 3, xmove, ymove);
                }
                else if (castleRight) {
                    swapPiece(xpiece, 6, xpiece, ypiece);
                    swapPiece(xmove, 5, xmove, ymove);
                }
                else {
                    swapPiece(xmove, ymove, xpiece, ypiece);
                }
            }
            else if (test.Team2->at(j)->inCheck(test.Team2->at(j)->getX(), test.Team2->at(j)->getY(), test.Team1)) {
                cout << "YOU PUT THIS MAN IN CHECK" << endl;
                checked = true;
            }
        }
        else {
            if (test.Team2->at(j)->inCheck(test.Team2->at(j)->getX(), test.Team2->at(j)->getY(), test.Team1)) {
                validMove = false;
                cout << "YOU PUT YOURSELF IN CHECK" << endl;
                if (castleLeft) {
                    swapPiece(2, ypiece, xpiece, ypiece);
                    swapPiece(3, ymove, xmove, ymove);
                }
                else if (castleRight) {
                    swapPiece(6, ypiece, xpiece, ypiece);
                    swapPiece(5, ymove, xmove, ymove);
                }
                else {
                    swapPiece(xmove, ymove, xpiece, ypiece);
                }
            }
            else if (test.Team1->at(j)->inCheck(test.Team1->at(j)->getX(), test.Team1->at(j)->getY(), test.Team2)) {
                cout << "YOU PUT THIS MAN IN CHECK" << endl;
                checked = true;
            }
        }
        if (checked && test.turn2) {
            if (isCheckMate(test.Team1, test.Team2)) {
                cout << "CHECKMATE SERGEI" << endl;
                test.hasWon = true;
            }
        }
        else if (checked && test.turn1) {
            if (isCheckMate(test.Team2, test.Team1)) {
                cout << "CHECKMATE SERGEI" << endl;
                test.hasWon = true;
            }
        }
        if (validMove && test.turn1 && chessboard[xmove][ymove]->abbr == 'P' && chessboard[xmove][ymove]->getX() == 7) {
            char newpiece;
            int index;
            cout << "enter what you want your pawn replaced with (upper case)!" << endl;
            cin >> newpiece;
            for (index = 0; index < 16; index++) {
                if (test.Team1->at(index) == chessboard[xmove][ymove]) {
                    break;
                }
            }
            chessboard[xmove][ymove]->pawnUpgrade(xmove, ymove, newpiece);
            test.Team1->at(index) = chessboard[xmove][ymove];
        }
        if (validMove && test.turn2 && chessboard[xmove][ymove]->abbr == 'p' && chessboard[xmove][ymove]->getX() == 0) {
            char newpiece;
            int index;
            cout << "enter what you want your pawn replaced with (upper case)!" << endl;
            cin >> newpiece;
            for (index = 0; index < 16; index++) {
                if (test.Team2->at(index) == chessboard[xmove][ymove]) {
                    break;
                }
            }
            chessboard[xmove][ymove]->pawnUpgrade(xmove, ymove, newpiece);
            test.Team2->at(index) = chessboard[xmove][ymove];
        }
        if (!test.hasWon) {
            printBoard();
        }
        if (validMove) {
            if (test.turn1 == true) {
                test.turn1 = false;
                test.turn2 = true;
            }
            else {
                test.turn1 = true;
                test.turn2 = false;
            }
        }
    }
    test.resetGame();
    printBoard();
    test.startGame();
    //	test.saveState();
    return 0;
}

 */
